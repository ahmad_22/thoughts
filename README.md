## About
<h2>Step 1: download in your system.</h2>  

git clone https://gitlab.com/ahmad_22/thoughts.git

cd Toughts

composer install

<h2>Step 2: Configure your database from .env file</h2> 

DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=thoughts

DB_USERNAME=root

DB_PASSWORD=

<h2>Step 3: Install composer</h2> 

composer install

php artisan migrate 
or
php artisan migrate:fresh

php artisan db:seed

<h2>Step 4: Run server</h2>  

php artisan serve


## gmail
google gmail

(ahmadouf22@gmail.com)

## phone

(0991272866)

## Facebook
Facebook

(https://www.facebook.com/ahmad.helale.52)

## Linkedin
Join in Linkedin

(https://www.linkedin.com/in/ahmad-helali-8b3650217/)
