<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Foreignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->foreignId('user_id')->nullable()->constrained('users','id');
        });
        Schema::table('paragraphs', function (Blueprint $table) {
            $table->foreignId('blog_id')->constrained('blogs','id');
        });
        Schema::table('photos', function (Blueprint $table) {
            $table->foreignId('paragraph_id')->constrained('paragraphs','id');
        });
        Schema::table('blog_tags', function (Blueprint $table) {
            $table->foreignId('tag_id')->constrained('tags','id');
            $table->foreignId('blog_id')->constrained('blogs','id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
