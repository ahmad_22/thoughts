<?php

namespace Database\Seeders;

use App\Models\Photo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            ['tag' => 'tag1'],
            ['tag' => 'tag2'],
            ['tag' => 'tag3'],
            ['tag' => 'tag4'],
            ['tag' => 'tag5'],
            ['tag' => 'tag6'],
        ]);
    }
}
