<?php

namespace Database\Seeders;

use App\Models\Paragraph;
use Illuminate\Database\Seeder;

class ParagraphSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paragraph::factory()->count(6)->create();
    }
}
