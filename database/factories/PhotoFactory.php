<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PhotoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'img_url'=> $this->faker->imageUrl(600, 300,),
            'paragraph_id'=>$this->faker->numberBetween(1,6),
        ];
    }
}
