<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class paragraphFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'text'=>$this->faker->text(500),
            'blog_id'=>$this->faker->numberBetween(1,3),
        ];
    }
}
