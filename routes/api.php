<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\PhotoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    $A=['qqqqqqqq','wwwwwwwwww'];
    return  response()->json($A);
});

Route::get('/test2', function () {
    $A='rrrrrrrrrrrrrrrrr';
    return  response()->json($A);
});
