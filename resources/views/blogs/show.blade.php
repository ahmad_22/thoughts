@extends('blogs.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('blogs.index') }}"> Back</a>
            </div>
        </div>
    </div>
<br>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <h5><strong>tags:</strong>
                @if(!blank($blog->tags))
                    @foreach ($blog->tags as $tag)
                        {{$tag->tag}}
                    @endforeach
                @endif
            </h5>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h1><strong>Name:</strong>
                {{ $blog->name }}</h1>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $blog->description }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <img height="300" width="600" src={{ $blog->img_caver }}>
            </div>
        </div>

        @foreach ($blog->parageaph as $item )
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    {{ $item->text }}
                </div>
            </div>
            @foreach ($item->photo as $img)
                @if ($img!=[])
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <img src={{ $img->img_url }}>
                        </div>
                    </div>
                @endif
            @endforeach
        @endforeach
    </div>
@endsection
