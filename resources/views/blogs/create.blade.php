@extends('blogs.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Create New Blog</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('blogs.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Warning!</strong> Please check your input code<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>name:</strong>
                <input required type="text" name="name" id="name" class="form-control" placeholder="name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>tags:</strong>
                <input type="text" class="form-control" id="tags_name" readonly>
                <input type="text" hidden id="tag_ids">
                <select name="tags" class="form-control" id="tags">
                    <option value=""></option>
                    @foreach ($tags as $tag)
                        <option value="{{$tag->id}}">{{$tag->tag}}</option>
                    @endforeach
                  </select>
            </div>
            <div class="form-group">
                <strong>Description:</strong>
                <textarea required class="form-control" id="description" style="height:50px" name="description" placeholder="Description"></textarea>
                <strong>Add caver photo:</strong>
                <input required id="img_caver"  type="file" name="img_caver" class="btn btn-primary" value="Add caver photo">
                <input type="text" hidden id="url_img_caver">
                <div id="paragraphs">
                    <div class="paragraphLength" ><br>
                        <strong>paragraph:</strong>
                        <textarea  required class="form-control text" style="height:100px" name="text" placeholder="paragraph" ></textarea>
                        <input multiple required id="img_url"  type="file" name="img_url" class="btn btn-primary" value="Add photo">
                        <input type="text" hidden class="urls">
                    </div>
                </div>
                <input type="button" class="btn btn-success" onclick="addparagraph()" value="Add paragraph">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                <button onclick="submit()" type="submit" class="btn btn-primary">Submit</button>
                <meta name="csrf-token" content="{{ csrf_token() }}">

        </div>
    </div>

<script>
    var tag_ids = new Array();
    $(document).on('change','#tags',(e)=>{
        var tag=document.getElementById('tags').value;
        var tag_select=$( "#tags option:selected" ).text();
        var tags_name=document.getElementById('tags_name').value;
        if(tag != ''){
            $('#tags_name').val(tag_select+' '+tags_name);
            tag_ids.push(tag);
        }
    });

    $(document).on('change','#img_caver',(e)=>{
        var img=e.target.files[0];
        var files = new FormData();
        files.append('imgs[]',img);
        $.ajax({
            url:'/uploadImage',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            dataType :"json",
            data: files,
            contentType: false,
            processData: false,
        }).done((url)=>{
            console.log(url);
            $('#url_img_caver').val(url);
        })
    });

    $(document).on('change','#img_url',(e)=>{
        var imgs=e.target.files;
        console.log(imgs[0]);
        var files = new FormData();
        for (var x = 0; x < imgs.length ; x++) {
            files.append("imgs[]", imgs[x]);
        }
        $.ajax({
            url:'/uploadImage',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data: files,
            processData: false,
            contentType: false,
        }).done((url)=>{
            console.log(url);
            var row = $(e.target);
            row.closest('.paragraphLength').find('.urls').val(url);
        });
    });

    function addparagraph() {
        $('#paragraphs').append(`
            <div class="paragraphLength" >
            <strong>paragraph:</strong>
            <textarea required class="form-control text" style="height:100px" name="text" placeholder="paragraph" ></textarea>
            <input multiple required  id="img_url"  type="file" name="img_url" class="btn btn-primary" value="Add photo">
            <input type="text" hidden class="urls">
            </div>
        `)
    }

    function  submit(){
        var blog = new Array();
        blog['name']=document.getElementById('name').value;
        blog['description']=document.getElementById('description').value;
        blog['img_caver']=document.getElementById('url_img_caver').value;
        var paragraph=[]
        var urls = document.getElementsByClassName('urls');
        var texts = document.getElementsByClassName('text');
        for (let index = 0; index < urls.length; index++) {
            const element = urls[index];
            const pp = texts[index];
            paragraph.push({
                text:pp.value,
                img_urls:element.value.split(','),
            });
        }
        blog['paragraph'] =paragraph;
        $.ajax({
        url:'/blogs',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        dataType :"json",
        data:{
            name:blog['name'],
            tag_ids:tag_ids,
            description:blog['description'],
            img_caver:blog['img_caver'],
            paragraphs:blog['paragraph']
        },
        }).done((url)=>{
            window.location.href = "/blogs";
        });
    }
</script>
@endsection
