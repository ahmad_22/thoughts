@extends('blogs.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Blog</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('blogs.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> Please check input field code<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>name:</strong>
                    <input type="text" name="name" id="name" value="{{ $blog->name }}" class="form-control" placeholder="name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>tags:</strong>
                    <input type="button"  id="clear_tags" value="clear" onclick="clear_tag()">
                    <input type="text" class="form-control" id="tags_name" readonly>
                    <input hidden type="text" id="blog_tag" value={{$blog->tags}} readonly>
                    <select name="tags" class="form-control" id="tags">
                        <option value=""></option>
                        @foreach ($tags as $tag)
                            <option value="{{$tag->id}}">{{$tag->tag}}</option>
                        @endforeach
                      </select>
                </div>
                <div class="form-group">
                    <strong>Description:</strong><strong>Add caver photo:</strong>
                    <textarea class="form-control" style="height:50px" name="description" id="description" placeholder="Description">{{ $blog->description }}</textarea>
                    <input required id="img_caver"  type="file" name="img_caver" class="btn btn-primary" value="Add caver photo">
                    <input type="text" hidden id="url_img_caver" value="{{ $blog->img_caver }}">you have caver photo need to change it <br>
                    <div id="paragraphs">
                        <div class="paragraphLength" >
                            @php($urls='')
                            @foreach ($blog->parageaph as $item )
                                <strong>paragraph:</strong>
                                <textarea class="form-control text" style="height:100px" name="text" placeholder="paragraph" >{{ $item->text }}</textarea>
                                <input multiple  id="img_url"  type="file" name="img_url" class="btn btn-primary" value="Add photo">
                                @if (!blank($item->photo))
                                    @foreach ($item->photo as $imgs )
                                    @php($urls .=$imgs->img_url . ',')
                                    @endforeach
                                @endif
                                <input type="text" hidden class="urls" value="{{$urls}}">
                                you have {{count($item->photo)}} photo do you need to change<br>
                                @php($urls='')
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                <button onclick="submit()" type="submit" class="btn btn-primary">Edit</button>
                <meta name="csrf-token" content="{{ csrf_token() }}">
            </div>
        </div>

<script>

var tag_ids = new Array();

$( document ).ready(function() {
    var tags = {!! json_encode($blog->tags) !!}
    for (let i = 0; i < tags.length ; i++) {
        var tags_name=document.getElementById('tags_name').value;
        $('#tags_name').val(tags_name +' '+ tags[i]['tag']);
        tag_ids.push(tags[i]['id']);
    }
});

function clear_tag(){
    $('#tags_name').val('');
    tag_ids=[];
}

    $(document).on('change','#tags',(e)=>{
        var tag=document.getElementById('tags').value;
        var tag_select=$( "#tags option:selected" ).text();
        var tags_name=document.getElementById('tags_name').value;
        if(tag != ''){
            $('#tags_name').val(tag_select+' '+tags_name);
            tag_ids.push(tag);
        }
    });

$(document).on('change','#img_caver',(e)=>{
        var img=e.target.files[0];
        var files = new FormData();
        files.append('imgs[]',img);
        $.ajax({
            url:'/uploadImage',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            dataType :"json",
            data: files,
            contentType: false,
            processData: false,
        }).done((url)=>{
            console.log(url);
            $('#url_img_caver').val(url);
        })
    });

    $(document).on('change','#img_url',(e)=>{
        var imgs=e.target.files;
        console.log(imgs[0]);
        var files = new FormData();
        for (var x = 0; x < imgs.length ; x++) {
            files.append("imgs[]", imgs[x]);
        }
        $.ajax({
            url:'/uploadImage',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data: files,
            processData: false,
            contentType: false,
        }).done((url)=>{
            console.log(url);
            var row = $(e.target);
            row.closest('.paragraphLength').find('.urls').val(url);
        });
    });

    function  submit(){
        var name=document.getElementById('name').value;
        var description=document.getElementById('description').value;
        var img_caver=document.getElementById('url_img_caver').value;
        var paragraph=[];
        var urls = document.getElementsByClassName('urls');

        var texts = document.getElementsByClassName('text');
        for (let index = 0; index < urls.length; index++) {
            const element = urls[index];
            const pp = texts[index];
            paragraph.push({
                text:pp.value,
                img_urls:element.value.split(','),
            });
        }
        console.log(paragraph);
        $.ajax({
        url:'/blogs/{{$blog->id}}',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType :"json",
        method:"put",
        data:{
            name:name,
            description:description,
            img_caver:img_caver,
            paragraphs:paragraph
        },
        }).done((url)=>{
            window.location.href = "/blogs";
        });
    }
</script>
@endsection
