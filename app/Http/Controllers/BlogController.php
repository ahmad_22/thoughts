<?php

namespace App\Http\Controllers;

use App\Http\Requests\DestroyRequest;
use App\Http\Requests\ShowRequest;
use App\Http\Requests\StoreRequest;
use App\Http\Requests\UpdateRequest;
use App\Models\Blog;
use App\Models\BlogTag;
use App\Models\Paragraph;
use App\Models\Photo;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    private $blog, $paragraph, $pohto;

    public function __construct(Blog $blog, Paragraph $paragraph, Photo $pohto)
    {
        $this->blog = $blog;
        $this->paragraph = $paragraph;
        $this->pohto = $pohto;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blogs = $this->blog->all();
        return view('blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags=(new Tag())->all();
        return view('blogs.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $blogData = $request->only('name', 'description', 'img_caver');
        $blogCreated = $this->blog->createData($blogData);
        //paragraphs
        $paragraphs = $request->get('paragraphs');
        $this->blogDetail($blogCreated->id, $paragraphs);
        //tag
        if($request->filled('tag_ids')){
            $tagIds=$request->get('tag_ids');
            $this->tags($blogCreated->id,$tagIds);
        }

        return response()->json('created successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request)
    {
        $id = $request->validated();
        $blog = $this->blog->show($id['blog_id']);
        return view('blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ShowRequest $request)
    {
        $tags=(new Tag())->all();
        $id = $request->validated();
        $blog = $this->blog->show($id['blog_id']);
        return view('blogs.edit', compact('blog','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        $data = $request->validated();
        $blogId = $data['blog_id'];
        unset($data['blog_id']);
        $blogData = $request->only('name', 'description', 'img_caver');
        $blogCreated = $this->blog->updateData(['id'=>$blogId],$blogData,);

        $paragraphs = $request->get('paragraphs');
        $this->blogDetail($blogId, $paragraphs, true);
        //tag
        if($request->filled('tag_id'))
            $this->tags($blogCreated->id,$request->get('tag_id'),true);

        return response()->json('updated successfully', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request)
    {
        $id = $request->validated();
        $this->deleteDetail($id['blog_id']);
        $this->blog->deleteData(['id'=>$id['blog_id']]);
        return redirect()->route('blogs.index')
            ->with('success', 'Blog deleted successfully');
    }

    public function blogDetail($blogId, $paragraphs, $isUpdate = false)
    {
        if ($isUpdate) $this->deleteDetail($blogId);

        foreach ($paragraphs as $paragraph) {
            $paragraphData['text'] = $paragraph['text'];
            $paragraphData['blog_id'] = $blogId;
            $paragraphCrrated = $this->paragraph->createData($paragraphData);
            if (!blank($paragraph['img_urls'])) {
                $photoData = [];
                foreach ($paragraph['img_urls'] as $imgUrl) {
                    if (!blank($imgUrl)){
                        $temp['img_url'] = $imgUrl;
                        $temp['paragraph_id'] = $paragraphCrrated->id;
                        $temp['created_at'] = Carbon::now();
                        $temp['updated_at'] = Carbon::now();
                        array_push($photoData, $temp);
                    }
                }
                $this->pohto->insertData($photoData);
            }
        }
        return true;
    }

    public function deleteDetail($blogId)
    {
        $paragraphs = $this->paragraph->getData(['blog_id' => $blogId]);
        if(!blank($paragraphs)){
            $paragraphIds = $paragraphs->pluck('id')->toArray();
            $find= $this->pohto->getData($paragraphIds);
            if(!blank($find))
                $this->pohto->deleteData( $paragraphIds);
            $this->paragraph->deleteData(['blog_id' => $blogId]);
        }
        return true;
    }

    public function tags($blogId, $tagsIds, $isUpdate=false)
    {
        if($isUpdate) (new BlogTag())->deleteData($tagsIds);

        $blogTagData=[];
        foreach($tagsIds as $id){
            $temp['blog_id']=$blogId;
            $temp['tag_id']=$id;
            $temp['created_at']=Carbon::now();
            $temp['updated_at']=Carbon::now();
            array_push($blogTagData,$temp);
        }
        (new BlogTag())->insertData($blogTagData);
        return true;
    }
}
