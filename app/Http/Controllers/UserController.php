<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user, $blog;

    public function __construct(User $user,Blog $blog)
    {
        $this->user = $user;
        $this->blog = $blog;
    }

    public function login(LoginRequest $request)
    {
        $data=$request->only('email', 'password');
        $userLogin = $this->user->login($data);
        if (empty($userLogin))
            return back();
        $blogs = $this->blog->userBlog($request->user()->id);
        return view('blogs.index', compact('blogs'));
    }

    public function all()
    {
        $blogs = $this->blog->all();
        return view('blogs.index', compact('blogs'));
    }
}
