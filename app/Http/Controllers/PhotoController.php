<?php

namespace App\Http\Controllers;

use App\Classes\FileHelper;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function uploadImage(Request $request)
    {
        $imgs=$request->file('imgs');
        $url=null;
        foreach($imgs as $img)
            $url .= URL::to('/').'/storage/app'. $uploadedFileResult = FileHelper::uploadImage($img).',';
        return response()->json($url);
    }
}
