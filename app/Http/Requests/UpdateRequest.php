<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function validationData()
    {
        return array_merge($this->all(), $this->route()->parameters());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //info blog
            'blog_id' => ['required', 'integer', Rule::exists('blogs', 'id')
                ->whereNull('deleted_at')],
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'img_caver' => ['required', 'string'],
            //paragraph text && photo img_url
            'paragraphs'=>['required', 'array'],
            'paragraphs.*.text'=>['required','string'],
            'paragraphs.*.img_url'=>['nullable','array'],
            'paragraphs.*.img_url.*'=>['nullable','string'],
            //tag
            'tag_id'=>['nullable','array'],
            'tag_id.*'=>['nullable','integer',Rule::exists('tags','id')
                ->whereNull('deleted_at')],
        ];
    }
}
