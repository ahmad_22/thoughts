<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //info blog
            'name' => ['required', 'string', 'max:50'],
            'description' => ['required', 'string', 'max:200'],
            'img_caver' => ['required', 'string'],
            //paragraph text && photo img_url
            'paragraphs'=>['required', 'array'],
            'paragraphs.*.text'=>['required','string'],
            'paragraphs.*.img_url'=>['nullable','array'],
            'paragraphs.*.img_url.*'=>['string'],
            //tag
            'tag_ids'=>['nullable','array'],
            'tag_ids.*'=>['nullable','integer',Rule::exists('tags','id')
                ->whereNull('deleted_at')],
        ];
    }
}
