<?php


namespace App\Classes;

use Exception;
use Illuminate\Support\Facades\Storage;

class FileHelper
{
    public static function uploadImage($file, $disk = null)
    {
        try {
            $fileName = time() . '.' . $file->extension();
            $path = '/img/blog/';
            $disk = $disk ?? config('filesystems.default');

            Storage::disk($disk)
                ->put($path . $fileName, (string)$file, 'public');
        } catch (Exception $exception) {
            return null;
        }
        return $data = config("filesystems.disks.{$disk}.url") . $path . $fileName;
    }
}
