<?php

namespace App\Models;

use App\Trait\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'blogs';

    protected $fillable = ['name', 'description', 'img_caver', 'user_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

    public function parageaph()
    {
        return $this->hasMany(Paragraph::class);
    }

    public function tags()
    {
        return $this->hasManyThrough(
            Tag::class,
            BlogTag::class,
            'blog_id',
            'id',
            'id',
            'tag_id'
        );
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userBlog($userId)
    {
        return $this->where('user_id', $userId)->get();
    }

    public function show($blogId)
    {
        return $this->where('id', $blogId)->with('parageaph.photo','tags')->first();
    }
}
