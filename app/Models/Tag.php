<?php

namespace App\Models;

use App\Trait\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'tags';

    protected $fillable = ['tag'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

}
