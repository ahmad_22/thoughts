<?php

namespace App\Models;

use App\Trait\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogTag extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'blog_tags';

    protected $fillable = ['blog_id','tag_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function deleteData($blogIds)
    {
        return $this->whereIn('blog_id',$blogIds)->delete();
    }
}
