<?php

namespace App\Models;

use App\Trait\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paragraph extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'paragraphs';

    protected $fillable = ['text', 'blog_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

    public function photo()
    {
        return $this->hasMany(Photo::class);
    }

    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }

    public function getData($filter = [])
    {
        return $this->where($filter)->get();
    }

}
