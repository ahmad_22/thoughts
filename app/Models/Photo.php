<?php

namespace App\Models;

use App\Trait\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'photos';

    protected $fillable = ['img_url', 'paragraph_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $dates = [];

    protected $casts = [];

    public function paragraph()
    {
        return $this->belongsTo(Paragraph::class);
    }

    public function deleteData($paragraphIds)
    {
        return $this->whereIn('paragraph_id',$paragraphIds)->delete();
    }

    public function getData($paragraphIds)
    {
        return $this->whereIn('paragraph_id',$paragraphIds)->get();
    }
}
