<?php

namespace App\Models;

use App\Trait\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, ModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['name', 'email', 'password', 'photo'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = ['email_verified_at' => 'datetime',];

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }

    public function createData($data)
    {
        return $this->create($data);
    }

    public function updateData($filter = [], $data)
    {
        return $this->where($filter)->update($data);
    }

    public function deleteData($filter = [])
    {
        return $this->where($filter)->delete();
    }

    public function login($data)
    {
        $user = null;
        if (Auth::attempt($data)) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('thoughts')->plainTextToken;
            $success['name'] =  $user->name;
        }
        return $user;
    }
}
